package com.is.fixerratescheduler.repository;

import com.is.fixerratescheduler.entity.ExchangeRateEntity;
import com.is.fixerratescheduler.entity.ExchangeRateId;
import org.springframework.data.repository.CrudRepository;

public interface ExchangeRateRepository extends CrudRepository<ExchangeRateEntity, ExchangeRateId> {
}
