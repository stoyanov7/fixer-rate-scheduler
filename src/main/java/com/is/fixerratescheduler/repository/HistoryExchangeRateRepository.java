package com.is.fixerratescheduler.repository;

import com.is.fixerratescheduler.entity.HistoryExchangeRateEntity;
import com.is.fixerratescheduler.entity.HistoryExchangeRateId;
import org.springframework.data.repository.CrudRepository;

public interface HistoryExchangeRateRepository extends CrudRepository<HistoryExchangeRateEntity, HistoryExchangeRateId> {
}
