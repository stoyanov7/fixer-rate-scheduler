package com.is.fixerratescheduler.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ConfigurationResolver {
    @Value("${rates.api-key}")
    private String apiKey;

    @Value("${rates.latest.base}")
    private String baseCurrencies;
}
