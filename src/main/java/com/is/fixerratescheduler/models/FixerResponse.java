package com.is.fixerratescheduler.models;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Map;

@Getter
@Setter
public class FixerResponse {
    private String base;
    private LocalDate date;
    private Map<String, Double> rates;
    private Boolean success;
    private Long timestamp;
}
