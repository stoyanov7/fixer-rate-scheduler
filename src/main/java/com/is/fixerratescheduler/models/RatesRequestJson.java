package com.is.fixerratescheduler.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RatesRequestJson {
    @JsonProperty("currency")
    public String currency;

    @JsonProperty("timestamp")
    public Long timestamp;
}
