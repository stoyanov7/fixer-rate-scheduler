package com.is.fixerratescheduler.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Table(name = "exchange_rate")
@Entity
@IdClass(ExchangeRateId.class)
@Getter
@Setter
public class ExchangeRateEntity {
    @Id
    @Column(name = "base")
    private String base;

    @Id
    @Column(name = "quote")
    private String quote;

    @Column(name = "rate")
    private String rate;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    public ExchangeRateEntity() {}

    public ExchangeRateEntity(String base, String quote, String rate, LocalDateTime timestamp) {
        this.base = base;
        this.quote = quote;
        this.rate = rate;
        this.timestamp = timestamp;
    }
}
