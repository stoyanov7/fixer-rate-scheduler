package com.is.fixerratescheduler.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Setter
@Getter
public class ExchangeRateId implements Serializable {

    private String base;
    private String quote;

    public ExchangeRateId(){}

    public ExchangeRateId(String base, String quote) {
        this.base = base;
        this.quote = quote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        ExchangeRateId that = (ExchangeRateId) o;
        return base.equals(that.base) &&
                quote.equals(that.quote);
    }

    @Override
    public int hashCode() {
        return Objects.hash(base, quote);
    }
}
