package com.is.fixerratescheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FixerRateSchedulerApplication {
	public static void main(String[] args) {
		SpringApplication.run(FixerRateSchedulerApplication.class, args);
	}
}
