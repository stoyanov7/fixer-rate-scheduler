package com.is.fixerratescheduler.service;

import com.is.fixerratescheduler.config.ConfigurationResolver;
import com.is.fixerratescheduler.entity.ExchangeRateEntity;
import com.is.fixerratescheduler.entity.HistoryExchangeRateEntity;
import com.is.fixerratescheduler.models.FixerResponse;
import com.is.fixerratescheduler.repository.ExchangeRateRepository;
import com.is.fixerratescheduler.repository.HistoryExchangeRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class FixerClientImpl implements FixerClient {
    private static final String FIXER_URL = "https://api.apilayer.com/fixer/latest?base=";

    private final RestTemplate restTemplate;
    private final ExchangeRateRepository exchangeRateRepository;
    private final HistoryExchangeRateRepository historyRepository;
    private final ConfigurationResolver configurationResolver;

    @Autowired
    public FixerClientImpl(RestTemplate restTemplate,
                           ExchangeRateRepository exchangeRateRepository,
                           HistoryExchangeRateRepository historyRepository,
                           ConfigurationResolver configurationResolver) {
        this.restTemplate = restTemplate;
        this.exchangeRateRepository = exchangeRateRepository;
        this.historyRepository = historyRepository;
        this.configurationResolver = configurationResolver;
    }

    @Scheduled(fixedDelayString = "${rates.poll.rate}")
    public void syncLatestRatesByCurrency() {
        HttpEntity<Void> requestEntity = buildHttpEntity();
        String[] bases = this.configurationResolver.getBaseCurrencies().split(",");
        ExecutorService pool = Executors.newFixedThreadPool(128);

        Arrays.stream(bases)
            .map(String::trim)
            .filter(b -> !b.isEmpty())
            .forEach(base ->
                pool.execute(() -> {
                    ResponseEntity<FixerResponse> response = restTemplate.exchange(FIXER_URL + base, HttpMethod.GET, requestEntity, FixerResponse.class);
                    if (response.getBody() != null) {
                        saveRates(response.getBody());
                    }
                }
            ));
    }

    private HttpEntity<Void> buildHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("apikey", this.configurationResolver.getApiKey());
        return new HttpEntity<>(headers);
    }

    @Transactional
    protected void saveRateData(ExchangeRateEntity exRate) {
        this.exchangeRateRepository.save(exRate);
        this.historyRepository.save(new HistoryExchangeRateEntity(exRate));
    }

    private void saveRates(FixerResponse response) {
        response
            .getRates()
            .forEach((key, value) -> {
                LocalDateTime timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(response.getTimestamp()), TimeZone.getTimeZone(ZoneOffset.UTC).toZoneId());
                ExchangeRateEntity exRate = new ExchangeRateEntity(response.getBase(), key, value.toString(), timestamp);

                this.saveRateData(exRate);
            });
    }
}
