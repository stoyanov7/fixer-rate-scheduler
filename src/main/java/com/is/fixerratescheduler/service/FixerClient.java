package com.is.fixerratescheduler.service;

public interface FixerClient {
    void syncLatestRatesByCurrency();
}
